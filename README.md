# AdPro Group Project, Final Delivery 

The files "biker_class.py" and "Showcase.ipynb" are the Hackathon day 2 results of Group L. Changes of results of day 1 have been recorded in the file "Changelog".

We are participating in a two-day hackathon promoted to study the mobility of bicycle users. They send us, the best team of Data Scientists in the company's roster. By promoting cycling awareness, our company expects to contribute to the green transition.

The file biker_class.py contains a specific python __Class__ (Bikers) that handles the heavy work for us. The class has six methods, the first 2 methods focus on getting the data: 
The first method ("download") downloads the .zip file with the data into a __downloads/__ directory in the root directory of the project (main project directory). If the .zip file already exists, the method will not download it again.
The second method ("read") reads the .csv file with hourly aggregations inside the .zip file in the __downloads/__ directory into a pandas dataframe which will become an attribute of our __Class__. The .csv file with daily aggregations is also read into a dataframe that becomes an attribute of our __Class__. The index of both files is set to a datetime index, containing information on the date (and in case of the hourly date csv additionally the hour).

The next three methods focus on analysing our data:
The third method ("plot") to the __Class__ plots a correlation matrix of month, humidity, weather situation, temperature, windspeed, and total number of bike rentals.
The fourth method ("pick") allows us to pick a week number between 0 and 102 weeks. If the chosen number is not in the [0, 102] range the method raises a ValueError and warns the user of the allowed range. If everything is OK, the method then plots the chosen week with labels in the axis and also a title.
The fifth method ("avg_month") plots the average total rentals by month of the year.

The final method ("forecasting") 


Group Members:
<br>Lara Ohler (45886) - 45886@novasbe.pt
<br>Luisa Kraut (43992) - 43992@novasbe.pt
<br>Niklas Kügler (44386) - 44386@novasbe.pt
<br>Puya Gabari (44443) - 44443@novasbe.pt
<br>Tim Siegelkow (44144) - 44144@novasbe.pt



