"""
This module defines a class "Bikers" that handles the heavy work of analysing the biker dataset
"""

from urllib.request import urlretrieve
import os
import zipfile
from datetime import datetime, date
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

class Bikers:
    """
    This class has six different methods.
    The first method (download) contains a function to download & save zip files.
    The second method (read) reads the csv files inside the zip file into a
    pandas dataframe & sets a datetime index.
    The third method (plot) plots a correlation matrix.
    The fourth method (pick) plots bike rentals for a week chosen by user input.
    The fifth method (avg_month) plots the average total rentals by month of the year.
    The plot shows a barchart where the x-axis is the month and the y-axis is the
    total average rentals.
    The final method (forecast) plots the expected weekly rentals in a chosen month
    and a shaded area corresponding to an interval of [-1 std deviation, +1 std deviation].
    """

    # Attributes
    hourly_data = 0
    daily_data = 0

    @staticmethod
    def download():
        """
        Downloads a zip file and saves it to a downloads directory (in root directory of project)
        If the .zip file already exists, the method will not download it again.

        Parameters
        -----------------
        file_link: str
            A string containing the url of the zip file to be downloaded.
        output_file: str
            A string containing the name of the output file. The default value is 'file.csv'.

        Returns
        -----------------
        Nothing.
        """

        url = "https://archive.ics.uci.edu/ml/machine-learning-databases/00275/Bike-Sharing-Dataset.zip"

        target_file = "./downloads/biketrips.zip"

        if not os.path.exists(target_file):
            urlretrieve(url, filename=target_file)
        else:
            print("File already exists!")

    def read(self):
        """
        Reads the .csv file with hourly aggregations inside .zip file
        in the downloads/ directory into a pandas dataframe
        which will become an attribute of the Class.
        The index is set containing information on date and time of the concering row.

        Parameters
        -----------------
        output_file: string
            The name of the zip file to be unzipped.
        targetfile: string
            The name of the file in the zip to be read into a pandas dataframe.

        Returns
        -----------------
        Pandas dataframe of the csv file.
        """

        zip_contents = zipfile.ZipFile("./downloads/biketrips.zip")

        # Initiate custom date parser to create index column with data type "datetime"
        custom_date_parser = lambda x: datetime.strptime(x, "%Y-%m-%d %H")

        # Create Dataframe with hourly aggragations and "week column"
        df_hourly = pd.read_csv(zip_contents.open("hour.csv"), parse_dates=[['dteday', 'hr']],
            date_parser=custom_date_parser)

        week = 0

        for i in range(0, len(df_hourly) - 1):
            if (i > 0) and (i % 168 == 0):
                week += 1
            df_hourly.loc[i, "week"] = int(week)

        # Create Dataframe with daily aggragations and "week column"
        df_daily = pd.read_csv(zip_contents.open("day.csv"))

        week = 0

        for i in range(0, len(df_daily) - 1):
            if (i > 0) and (i % 7 == 0):
                week += 1
            df_daily.loc[i, "week"] = int(week)

        # Create Datetime Index
        df_hourly = df_hourly.set_index('dteday_hr')

        df_daily = df_daily.set_index(pd.DatetimeIndex(df_daily['dteday']))

        # Set class attributes
        self.daily_data = df_daily
        self.hourly_data = df_hourly

    def plot(self):
        """
        Plots a correlation matrix of month, humidity, weather situation,
        temperature, windspeed, and total number of bike rentals.

        Parameters
        -----------------
        df: string
            The name of the df file to be plotted.

        Returns
        -----------------
        Plot of the correlation matrix with title and lables on each axis.
        """

        corr = (
            self.hourly_data[["mnth", "hum", "weathersit", "temp", "windspeed", "cnt"]]
            .corr()
            .style.background_gradient(cmap="coolwarm")
        )
        return corr

    def pick(self):
        """
        Allows user to pick a week number between 0 and 102.
        For the chosen week, the bike rental counts are plotted.
        If the chosen number is not in the [0, 102] range,
        an error is raised and the user warned about the allowed range.

        Parameters
        -----------------
        a: int
            Number between 0 and 102 that indicates which week should be plotted.

        Returns
        -----------------
        Plot of the week chosen with title and lables on each axis.
        """

        df_hourly = self.hourly_data
        df_daily = self.daily_data

        week_choice = input("Please choose a week between 0 and 102: ")
        week_choice_int = int(week_choice)
        if week_choice_int not in range(0, 103):
            raise ValueError(
                "The allowed range is from 0 to 102. Please choose a week in this range."
            )
        if week_choice_int in range(0, 103):
            print(
                df_hourly[df_hourly["week"] == week_choice_int].plot(
                    x="instant",
                    y="cnt",
                    title="Bike Rentals in your chosen Week",
                    xlabel="Week in hours",
                    ylabel="Bike Rentals",
                    legend=False,
                    kind="bar",
                    xticks=[24, 48, 72, 96, 120, 144, 168],
                )
            )
            print(
                df_daily[df_daily["week"] == week_choice_int].plot(
                    x="instant",
                    y="cnt",
                    title="Bike Rentals in your chosen Week",
                    xlabel="Week in days",
                    ylabel="Bike Rentals",
                    legend=False,
                    kind="bar",
                    xticks=[0, 1, 2, 3, 4, 5, 6],
                )
            )
    
    def avg_month(self):
        """
        Plots the average total rentals by month of the year.

        Parameters
        -------------
        none

        Returns
        -------------
        Barchart where the x-axis is the month and the y-axis is the total average rentals.
        """

        self.daily_data.groupby(self.daily_data.index.month)['cnt'].mean().plot(
            kind='bar',
            ylabel="Average total bike rentals",
            xlabel="Month of the year")

    def forecast(self):
        """
        Allows user to pick a month as month number (1-12).
        If the chosen number is not in the [1, 12] range,
        an error is raised and the user warned about the allowed range.
        For the chosen month, a 168-hour period(a Mon-Sun week) corresponding to
        the average bike rental and a shaded area corresponding
        to an interval of [-1 std deviation, +1 std deviation] are plotted.

        Parameters
        -----------------
        a: int
            Number between 1 and 12 that indicates which Month should be plotted.

        Returns
        -----------------
        Plot of expected weekly rentals in the chosen month and a shaded area corresponding
        to an interval of [-1 std deviation, +1 std deviation].
        """

        df_hourly = self.hourly_data

        month_choice = input("Please choose a month (1-12)")
        month_choice_int = int(month_choice)
        month = date(2011, month_choice_int, 1).strftime('%B')
        if month_choice_int not in range(1, 12):
            raise ValueError("Please choose a month by number(int)")
        df_monthly = df_hourly[df_hourly["mnth"] == month_choice_int]
        df_avg_std = df_monthly.groupby(['weekday',df_monthly.index.hour]).agg(
            {'cnt': [np.mean, np.std]})
        df_avg_std['+std']= df_avg_std['cnt']['mean'] + df_avg_std['cnt']['std']
        df_avg_std['-std']= df_avg_std['cnt']['mean'] - df_avg_std['cnt']['std']
        plt.figure(figsize=(12,7))
        d_d = np.arange(0,168)
        bar_plot = df_avg_std['cnt']['mean']
        std_plot = plt.fill_between(d_d, df_avg_std['+std'], df_avg_std['-std'],facecolor='green',
            label="[-1 std, +1 std]", alpha=0.4)
        bar_plot.plot(kind='bar', ylabel="Number of expected bike rentals", xlabel="Day, Hour")
        plt.locator_params(axis="x", nbins=50)
        plt.title("Expected weekly rentals in {month}".format(month=month))
        plt.legend(handles=[std_plot])
        plt.show()
        
